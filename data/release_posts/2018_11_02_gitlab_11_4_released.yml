features:
# TOP FEATURE
  top:
    - name: "マージリクエストのレビュー機能"
      available_in: [premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/discussions/index.html'
      image_url: '/images/11_4/batch-comments.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/1984'
      description: |
        マージリクエストでのコードレビューはGitLabの機能の中でも強力な機能の一つです。
        チームメンバーで、コードの差分の特定行にコメントし、リンクすることができます。
        さらに、コメントした内容が解決したことをマークすることもできます。
        しかし、変更の差分が巨大になると、マージリクエストでのコードレビューには苦痛が伴います。
        変更が大きな場合は、レビュアーは10箇所以上の行にコメントすることも珍しくはありません。
        例えば、コードレビューを進めるにつれて最初のコメントが適切ではないことに気がついたとしても、
        不要なコメントは既に投稿済みです。
        結果として、マージリクエストの作者には不要な通知が大量に送られてしまいます。

        このリリースで、マージリクエストのレビュー機能が導入されました。
        これにより、レビュアーはコードレビューのために複数のコメントを下書きできるようになりました。
        レビュアーはコメントに一貫性があることを確認してから、まとめて投稿することができます。
        下書きはGitLabのサーバーに保存されているので、会社のデスクトップでレビューの下書きを作成し、
        外出中にレビューの続きをタブレットで行うということも可能です。　
        下書きが投稿されると、今までのように通常のコメントとして表示されます。
        マージリクエストのレビュー機能よって、チームメンバーはより柔軟にコードレビューを実施できるようになります。

        将来的に、下書きを投稿する前に投稿内容を[プレビュー](https://gitlab.com/gitlab-org/gitlab-ee/issues/4327){:target="_blank"}する機能の追加や
        すべての通知を[一つの通知にまとめる](https://gitlab.com/gitlab-org/gitlab-ee/issues/4326){:target="_blank"}改善が計画されています。

# PRIMARY FEATURES
  primary:
    - name: "アプリケーションのフィーチャーフラグの作成と切替(アルファ版)"
      available_in: [premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/operations/feature_flags.html'
      image_url: '/images/11_4/feature_flags.png'
      reporter: jlenny
      team: release
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/6220'
      description: |
        フィーチャーフラグを作成し管理する機能が追加されました。
        作成したフィーチャーフラグは、シンプルなAPIを通してアプリケーションから利用できます。
        これによって、GitLabのフィーチャーフラグを使用して、
        アプリケーションの動作を切り替えることができます。

        フィーチャーフラグはあなたのアプリケーションに機能の切替システムを提供します。
        フィーチャーフラグにより、本番環境にリリースする新機能のテストや制御が容易になります。
        フィーチャーフラグは、リスクの低減に役立ち、どの機能を有効にするのかを簡単に管理できるようになります。

        現在、この機能はアルファ版です。
        今後の改善のため、この機能を試してのフィードバックをお願いします。

    - name: "マージリクエストの変更されたファイルをツリー形式で表示"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-request-diff-file-navigation'
      image_url: '/images/11_4/file-tree.png'
      reporter: jramsay
      team: create
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/14249'
      description: |
        コードレビューはプロジェクトを成功させるための基本的なプラクティスですが、
        差分のフラットなリストからは、何が変更されたかを判別するのは困難です。

        ファイルツリーは、変更されたファイルと変更の大きさを俯瞰するのに役立ちます。
        また、検索バーを使用して、パスや拡張子でレビュー対象のファイルを絞り込むこともできます。
        これによって、レビュワーは自分が専門分野の対象に集中して、効率的にレビューすることができます。

    - name: "コード所有者をマージリクエストの承認者として提案"
      available_in: [starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#codeowners-file'
      image_url: '/images/11_4/suggest-approvers.png'
      reporter: jramsay
      team: create
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/5382'
      description: |
        あなたの変更をレビューするのに最適な人が誰なのか明確でない場合があります。
        これからは、マージリクエストを作成、もしくは編集する際に、
        コード所有者が承認者として提案されます。
        そのため、適切な人を担当者として割り当てるのが簡単になります。

        コード所有者を定義する機能は[GitLab 11.3](https://about.gitlab.com/2018/09/22/gitlab-11-3-released/#code-owners){:target="_blank"}で追加されました。
        今後のリリースで、コード所有者は[担当者の自動割当](https://gitlab.com/gitlab-org/gitlab-ee/issues/1012){:target="_blank"}や
        [承認者の要求](https://gitlab.com/gitlab-org/gitlab-ee/issues/4418){:target="_blank"}など
        マージリクエストのワークフローにさらに統合される計画です。

    - name: "ユーザープロフィールのデザインを変更"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/profile/#user-profile'
      image_url: '/images/11_4/user-profile-redesign.png'
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/49801'
      description: |
        あなたのユーザープロフィールページには、
        あなたが何に興味を持ち、何に取り組んでいるのかを知ることができる、
        シンプルな情報が表示されるべきです。

        このリリースでは、プロフィールページに概要タブを追加しました。
        概要には、省略された貢献グラフ、最近の活動、および最も関連のある個人プロジェクトが表示されます。

    - name: "ユーザーメニューにステータスの変更と表示を追加"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/profile/#current-status'
      image_url: '/images/11_4/user-menu-status.png'
      image_noshadow: true
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/49075'
      description: |
        [GitLab 11.2](https://about.gitlab.com/2018/08/22/gitlab-11-2-released/#personal-status-messages){:target="_blank"}で、
        ユーザーの現在の状況や気分や好きな動物などを共有できるように、
        個人のステータスメッセージが追加されました。

        このリリースで、ステータスの変更をよりシンプルにスムーズに行えるようになります。
        ユーザーメニューに新しく追加された「Set status」をクリックすると、
        モーダル画面が表示され、ステータスの変更と消去ができます。
        さらに、設定したステータスがユーザーメニューに表示されるようになりました。

        例えば、ステータスに"island"の絵文字と「夏休み(8月10日〜8月15)」とメッセージを設定することで、
        他のメンバーに夏休み中であることを知らせることができます。

    - name: "<code>.gitlab-ci.yml</code>をインクルードして利用できるプランをStarterからCoreへ変更"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/#include'
      image_url: '/images/11_4/verify-includes.png'
      reporter: jlenny
      team: verify
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/42861'
      description: |
        このリリースから、Coreプランでも`.gitlab-ci.yml`をインクルードして利用できるようになりました。
        これにより、すべてのユーザーがCI/CDパイプラインでスニペットを再利用できるようになります。

    - name: "特定のファイルやパスで変更が発生した場合にだけジョブを実行"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/#only-changes'
      image_url: '/images/11_4/verify-onlyexceptchanges.png'
      reporter: jlenny
      team: verify
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/19232'
      description: |
        `.gitlab-ci.yml`内に`only`/`except`のルールを記述することで、
        特定のファイルやパス(glob)で変更が発生した場合にだけジョブを実行できるようになりました。

        これによって、様々なアセットが含まれるリポジトリの場合に、
        変更されたファイルに基づいて必要なジョブだけを実行できるようになり、
        パイプラインの実行時間を短縮することができます。

    - name: "Auto DevOpsに定期的なインクリメンタルロールアウトを追加"
      available_in: [premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/topics/autodevops/#timed-incremental-rollout-to-production'
      image_url: '/images/11_4/timed_incremental_rollouts.png'
      reporter: jlenny
      team: release
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/7545'
      description: |

        これまでもAuto DevOpsでインクリメンタルロールを設定可能でしたが、
        さらにこのリリースから、定期的なインクリメンタルロールを設定可能になりました。
        定期的なインクリメンタルロールは、エラーが無ければ、一定のリズムで自動的にロールアウトを継続します。

    - name: "GitLabの管理するアプリがKubernetes RBACをサポート"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/clusters/#role-based-access-control-rbac'
      image_url: '/images/11_4/k8s-rbac.png'
      reporter: danielgruesso
      team: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/29398'
      description: |
        インフラを構築、および既存のインフラに接続する際に、セキュリティを高めることは非常に重要です。
        Kubernetesのリソースへの適切なアクセス制御を提供するために、
        Kubernetes 1.8でRole-based access control(RBAC)が安定版となりました。

        このリリースから、GitLabのKubernetesインテグレーションで、
        GKEにRBACが有効なクラスターの構築、およびRBACが有効な既存のクラスターに接続できるようになりました。
        これによって、あなたのインフラのセキュリティを高めることができます。

    - name: "Auto DevOpsがRBACをサポート"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/topics/autodevops/'
      image_url: '/images/11_4/autodevops-rbac.png'
      reporter: danielgruesso
      team: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/44597'
      description: |
        このリリースから、Auto DevOpsでRBACが有効なKubernetesクラスターを利用できます。

        Role-based access control(RBAC)は、Kubernetesクラスターを運用する際に、
        安定性、セキュリティ、および効率性を高めるために非常に重要なツールです。
        Auto DevOpsでRBACが有効なクラスターを利用することで、あなたのアプリケーションのインフラのセキュリティを高めることができます。

    - name: "Auto DevOpsでPostgreSQL DBの初期化とマイグレーションをサポート"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/topics/autodevops/#auto-deploy'
      image_url: '/images/11_4/db-migrations.png'
      reporter: danielgruesso
      team: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/48004'
      description: |
        Auto DevOpsを利用すると、あなたのアプリケーションを自動的にビルド、テスト、デプロイ、および監視できます。
        GitLab 11.4からはさらに、Auto DevOpsでPostgreSQL DBの初期化とマイグレーションをできるようになりました。

        プロジェクト変数として、PostgreSQL DBを初期化、またはマイグレーションするスクリプトを定義するだけで、Auto DevOpsが残りの処理を実行します。

# SECONDARY FEATURES
  secondary:
    - name: "購読中のラベル一覧"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/labels.html#searching-for-project-labels'
      image_url: '/images/11_4/subscribed-labels.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/50835'
      description: |
        GitLabのラベルは、課題、マージリクエスト、およびエピックに適用できるのでとても強力です。
        しかし、使用するラベルが増えるにつれて、ラベルの管理が煩雑になります。

        そのため、最近のリリースでプロジェクトのラベル一覧ページでラベルの検索をできるようにしました。
        このリリースではさらに、名前、作成日、および更新日での並べ替えできるようにしたことに加えて、
        購読中のラベル一覧を表示できるようにしました。
        この機能は、グループとプロジェクトのラベル一覧の両方で利用できます。

    - name: "WIPマージリクエストの絞り込み"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html'
      image_url: '/images/11_4/wip-filter.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/13650'
      description: |
        GitLabのマージリクエストは、チームメンバーがコードを通して共同作業するために、中核となる機能です。
        WIP(work in progres)機能は、作業中のコードをチームメンバーと共有し、まだマージすべきではないことを伝えるのに役立ちます。

        このリリースでは、グループレベルとプロジェクトレベルの両方のマージリクエストの一覧で、
        WIPとWIP以外のマージリクエストを絞り込む機能が追加されました。
        これにより、作業中のマージリクエストとレビューの最終段階にあるマージリクエストを素早く探せるようになります。

    - name: "あなたへの<code>@mentions</code>を区別して強調表示"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/issues/issues_functionalities.html#13-mentions'
      image_url: '/images/11_4/highlight-yourself-distinct.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/49036'
      description: |
        課題やマージリクエストでのディスカッションが長くなると、
        自分へのコメントを見つけるのが困難になります。

        このリリースから、あなた(ログイン中のユーザー)への`@mentions`の色を他の色で強調表示します。
        これは、自分へのコメントを素早く見つけるのに役立ちます。

    - name: "クリックでマークダウンのテーブルやリンクを挿入"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/markdown.html'
      image_url: '/images/11_4/link-table-markdown.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/29788'
      description: |
        GitLabでは、テキストを入力するほとんどの箇所でGitLab Flavored Markdown(GFM)を利用できます。
        GFMを使用すると、シンプルな文法でリッチな表示が可能になります。
        例えば、GFMを使用してテーブルを作成することができます。
        これまでは、GFMでテーブルを作成する作業は、大量の文字を手打ちする必要があるために苦痛を伴うものでした。
        同様に、GFMはURLのリンクもサポートしていますが、その文法を忘れてしまうと使用できませんでした。

        このリリースからは、GFMエディターのテーブルボタンをクリックすることで、自動的にテーブルを挿入できるようになりました。
        これにより、テーブルを作成する手間が軽減されます。
        この機能は、GitLabのすべての説明欄とコメント欄で利用できます。

        また、リンクボタンをクリックすると、リンク文法の雛形が生成されます。
        ここにリンクの名前を入力しURLを貼り付けることで、リンクを簡単に作成することができます。

        テーブルの挿入は[George Tsiolis](https://gitlab.com/gtsiolis){:target="_blank"}さんの貢献によるものです。

        URLリンクの挿入は[Jan Beckmann](https://gitlab.com/kingjan1999){:target="_blank"}さんの貢献によるものです。

    - name: "作成された課題をバーンダウンチャートに内包"
      available_in: [starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/milestones/burndown_charts.html'
      image_url: '/images/11_4/burndown-chart.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/6174'
      description: |
        バーンダウンチャートは、チームがマイルストーンの進捗を測るのに役立ちます。
        通常、作業のスコープはマイルストーンの開始前に決定されるべきものです。
        しかし、緊急のバグやセキュリティの修正など、ルールには重要な例外がつきものです。

        このリリースから、このようなマイルストーンの途中に作成された課題も、
        バーンダウンチャートに内包されるようになりました。

    - name: "課題APIのウェイトの値を拡張"
      available_in: [starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/api/issues.html'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/6822'
      description: |
        以前のリリースで、課題のウェイトの値としてゼロより大きい任意の整数を使用できるように拡張されました。

        このリリースから、課題APIでも同じようにウェイトの値にゼロ以上の任意の整数をを設定できるように拡張されました。

    - name: "クイックアクションでディスカッションをロック"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/quick_actions.html'
      image_url: '/images/11_4/lock-discussion-quick-action.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/39173'
      description: |
        課題やマージリクエストのディスカッションをロックする機能は、
        新しい課題やマージリクエストで対話するように促すのに役立ちます。
        また、不正利用や非生産的な振る舞いを制限するのにも利用できます。

        このリリースから、クイックアクションでディスカッションをロック、またはアンロックできるようになりました。

        この機能は[Mehdi Lahmam](https://gitlab.com/mehlah){:target="_blank"}さんの貢献によるものです。

    - name: "エピックをクローズ"
      available_in: [ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/group/epics/'
      image_url: '/images/11_4/close-epics.png'
      reporter: victorwu
      team: plan
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/7013'
      description: |
        課題やマージリクエストと同様に、エピックをクローズ、および再オープンできるようになりました。
        また課題一覧と同様に、エピック一覧にオープン、クローズ、すべてのタブが追加されました。
        これにより、エピックのすべての作業が完了後にエピックをクローズすることで、
        デフォルトのエピック一覧に完了したエピックは表示されなくなります。

        課題と同様に、エピックのクローズは画面のボタン以外に、クイックアクションやAPIからも可能です。

    - name: "管理者エリアの設定の構成を改善"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/user/admin_area/settings/'
      image_url: '/images/11_4/admin-area-settings.png'
      image_noshadow: true
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/44998'
      description: |
        GitLabには非常に多くの機能があるので、GitLabの管理業務は大変な作業になり得ます。

        このリリースでは、すべてのセクションを個別設定のサブページに移動させることで、管理者エリアでの設定作業のユーザー体験を改善しました。
        これによって管理者は詳細ページに、より短時間でアクセスできるようになります。

    - name: "プロジェクを人気順に並べ替え"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/search/#projects'
      image_url: '/images/11_4/explore-projects-by-stars.png'
      image_noshadow: true
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/47203'
      description: |
        GitLabは、あなたが関心のあるプロジェクトを見つけられるように最善を尽くします。
        このリリースから、プロジェクトの探索ページで、プロジェクトを人気順に並べ替えできるようになりました。

        この機能は[Jacopo Beschi](https://gitlab.com/jacopo-beschi){:target="_blank"}さんの貢献によるものです。

    - name: "プロジェクトの概要にプログラミング言語の使用率を表示"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/repository/#repository-languages'
      image_url: '/images/11_4/repo-code-language-percentage.png'
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/51457'
      description: |
        以前のリリースで、プロジェクトの概要ページに、使用しているプログラミング言語のおおよその割合が分かるバーを追加しました。

        GitLab 11.4では、さらにプログラム言語の使用率も表示するようにしました。

        この機能は[Johann Hubert Sonntagbauer](https://gitlab.com/johann.sonntagbauer){:target="_blank"}さんの貢献によるものです。

    - name: "二段階認証のリカバリーコードをダウンロード"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#recovery-codes'
      image_url: '/images/11_4/2fa-codes-download.png'
      reporter: akaemmerle
      team: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/47963'
      description: |
        ウェブアプリケーションのログインに二段階認証を利用することはデファクトスタンダードとなっています。
        GitLabでは、二段階認証を最初にセットアップした時に、アカウントへのアクセスを回復するためのリカバリーコードが表示されます。
        リカバリーコードは、スマートフォンなどを紛失して、二段階認証でのログインができなくなった時に必要なものです。

        このリリースから、「Download codes」ボタンをクリックしてリカバリーコードをテキストファイルとしてダウンロードできるようになりました。

        この機能は[Luke Picciau](https://gitlab.com/Qwertie)さんの貢献によるものです。

    - name: "管理エリアのRunner一覧をRunnerのタイプや状態で絞り込み"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/ci/runners/'
      image_url: '/images/11_4/runner-admin-filter.png'
      reporter: jlenny
      team: release
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/36781'
      description: |
        管理エリアのRunner一覧をRunnerのタイプや状態で絞り込みできるようになりました。
        これは、たくさんのRunnerを管理するにに役立ちます。

    - name: "Docker executerで対話的なウェブターミナルをサポート"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/ci/interactive_web_terminal/'
      image_url: '/images/11_4/verify-webterm.png'
      reporter: jlenny
      team: verify
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-runner/issues/3467'
      description: |
        CIを実行するDocker executorで対話的なウェブターミナルの機能をサポートしました。
        現在は、スクリプトの処理が完了すると、ただちにDockerのセッションが終了します。
        しかし、次のリリースでは[#3605](https://gitlab.com/gitlab-org/gitlab-runner/issues/3605){:target="_blank"}を解決することで、この動作が改善される見込みです。

    - name: "プランによって利用できないAuto DevOpsのジョブをスキップ"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/topics/autodevops/'
      reporter: danielgruesso
      team: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/48399'
      description: |
        GitLab 11.4から、利用中のプランを評価してAuto DevOpsのジョブをスキップするようにしました。
        これにより、利用できない機能(例えば、Ultimateのみで利用可能なSASTやDAST)がある場合に、Auto DevOpsのパイプライン処理が高速化されます。

        これには、パイプライン処理の時間を短縮するだけではなく、パイプラインに不要なジョブが表示されなくなるので、見やすくなる効果もあります。

    - name: "パイプラインのジョブを遅延実行"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/README.html#when-delayed'
      image_url: '/images/11_4/verify-delayedjobruns.png'
      reporter: jlenny
      team: verify
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/51352'
      description: |
        このリリースから、`.gitlab-ci.yml`の中で`when`キーワードを使用することで、
        ジョブの開始を遅延させることができるようになりました。
        遅延がない場合のジョブの開始時刻を起点として、遅延させる時間を指定することができます。
        これにより、例えば、定期的にインクリメンタルロールアウトを実行するといった処理が可能になります。

    - name: "NurtchとJupyterHubで対話的な運用マニュアル"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/clusters/#installing-applications'
      image_url: '/images/11_4/nurtch.png'
      reporter: danielgruesso
      team: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/45912'
      description: |
        対話的な運用マニュアルは、運用者がインフラの診断やデプロイなどの定型的なタスクを実行するのに、非常に便利なツールです。

        このリリースから、GitLabのKubernetesインテグレーションでインストールしたJupyterHubアプリケーションに、
        Nurtchの[Rubix library](https://github.com/amit1rrr/rubix){:target="_blank"}が内包されるようになりました。
        これにより、DevOpsの運用マニュアルを作成するためのシンプルな方法が提供されます。
        提供されるサンプル運用マニュアルを利用した、[運用例の動画](https://youtu.be/Q_OqHIIUPjE){:target="_blank"}をご覧いただけます。

    - name: "プロジェクトで許可/禁止するライセンスの管理"
      available_in: [ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html#manual-license-management'
      image_url: '/images/11_4/license_management.png'
      reporter: bikebilly
      team: secure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/5940'
      description: |
        開発者がプロジェクトで許可/禁止するライセンスを管理するのに、ライセンスポリシーが有効です。
        新しいライセンスが追加されると、マージリクエストのページで直接そのライセンスを許可/禁止することができます。
        しかし、プロジェクトのメンテナによっては、許可/禁止するライセンスを事前に決めたい場合があります。

        GitLab 11.4では、ライセンスポリシーに手動で禁止/許可するライセンスを追加できるようになりました。
        プロジェクトのメンテナは、**Settings > CI/CD > License Management**ページで、事前にライセンスポリシーを作成できます。

    - name: "メトリクスダッシュボードに警告の閾値を表示"
      available_in: [ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#setting-up-alerts-for-prometheus-metrics'
      image_url: '/images/11_4/alert_threshold.png'
      reporter: joshlambert
      team: monitor
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/6036'
      description: |
        GitLab 11.4から、メトリクスダッシュボードに設定した警告の閾値が表示されるようになりました。
        これにより、警告を発しているメトリクスを簡単に見分けることができます。

    - name: "Gitプロトコル v2"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/administration/git_protocol.html'
      reporter: jramsay
      team: create
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ce/issues/46555'
      description: |
        開発者は、現在のブランチがリモートブランチから遅れていないか確認するために、一日に何回もfetchを行います。
        Gitプロトコル v2は、Gitの[通信プロトコル](https://www.kernel.org/pub/software/scm/git/docs/technical/pack-protocol.html){:target="_blank"}のメジャーアップデートで、
        cloneやfetchやpushの際にクライアント(あなたのマシン)とサーバー(GitLab)がどのようにやり取りするかを定義します。
        新しい通信プロトコルはfetchコマンドのパフォーマンスを向上させるなど様々な改善がされています。

        以前のプロトコルでは、fetchコマンドのレスポンスにはレポジトリのすべての参照が含まれていました。
        例えば、一つのブランチを更新するためにfetchする(つまり`git fetch origin master`)と、すべての参照のリストが取得されてしまいます。
        巨大なプロジェクトでは、数十万の参照と数十メガバイトのデータ量となる場合もあります。

        Gitプロトコル v2はGit v2.18からサポートされ、オプトインで提供されます。
        グローバルに有効にするには、`git config --global protocol.version 2`を実行してください。

        なお、SSHのGitプロトコル v2は、まだGitLab.comでは有効になっていません。
        また、セルフホスティングのGitLabインスタンスで使用するには、手動で有効にする必要があります。

    - name: "管理エリアのGeoのUXを改善"
      available_in: [premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/administration/geo/replication/configuration.html'
      image_url: '/images/11_4/geo_admin_area_improvements.png'
      reporter: akaemmerle
      team: geo
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/7126'
      description: |
        Geo(GitLabのレプリケーション機能)の管理者として、セカンダリノードのセットアップと同期状態を把握することは非常に重要です。

        GitLab 11.4では、同期状態や検証の詳細を画面に表示することで、管理エリアのGeo関連のUXを改善しました。
        プライマリノードの管理エリアには、新しく「Projects」ボタンが追加され、セカンダリノードのプロジェクト一覧を素早く開くことができます。
        セカンダリノードには、新しく「All」タブが追加され、検証状態の概要を素早く確認できます。

        今後、[さらなるUXの改善](https://gitlab.com/groups/gitlab-org/-/epics/369){:target="_blank"}が計画されています。

    - name: "Omniauth GitLabのPrometheusを2.0にアップグレード"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#11-4'
      reporter: joshlambert
      team: monitor
      issue_url: 'https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2940'
      description: |
        [簡易的にインスタンスを監視](https://docs.gitlab.com/ee/administration/monitoring/prometheus/){:target="_blank"}できるように、
        Omnibus GitLabにはPrometheusが同梱されています。
        Prometheusチームは、[多くの点が改善](https://prometheus.io/blog/2017/11/08/announcing-prometheus-2-0/){:target="_blank"}された、
        新メジャーバージョンの2.x系をリリースしました。
        これらの改善には、パフォーマンスの改善やより効率的な時系列データベースフォーマットなどが含まれます。
        残念ながら、データベースのアーキテクチャの変更により、1.x系のデータベースフォーマットとの互換性はありません。
        GitLab 11.4では、OmnibusパッケージにPrometheus 2.4.2が同梱され、ユーザーはその恩恵を得ることができます。

        - GitLab 11.4以上の新規インストールではPrometheus 2.x系が使用されます。
        - 既存のインスタンスでは、Prometheusは自動アップグレードされません。新しく追加された`gitlab-ctl Prometheus-upgrade`コマンドで、
          [Prometheusのアップグレードとオプションでデータ移行](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#11-4){:target="_blank"}ができます。
          データ移行中はPrometheusが停止します。
        - GitLab 12.0ではPrometheusが2.0に自動的にアップグレードされる予定です。この自動アップデートでは、Prometheus 1.0のデータ移行は行なわれない予定です。

        Prometheus 2.4.2へのアップグレードに関する詳細な情報が必要な場合は、[アップデートドキュメント](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#11-4){:target="_blank"}を参照してください。

    - name: "Geoの改善"
      available_in: [premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/administration/geo/replication/configuration.html'
      reporter: akaemmerle
      issueboard_url: 'https://gitlab.com/groups/gitlab-org/-/boards/796982?scope=all&utf8=✓&state=opened&label_name[]=Geo&milestone_title=11.4'
      description: |
        GitLabでは、分散チーム向けの機能の[Geo](https://about.gitlab.com/features/gitlab-geo)の改善を継続して行っています。
        GitLab 11.4では注目すべきいくつかの改善があります:

        - [パフォーマンスの大きな改善](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=closed&label_name%5B%5D=Geo%20Performance&milestone_title=11.4){:target="_blank"}
        - [keep-around参照をチェックサムの計算に含めるように変更](https://gitlab.com/gitlab-org/gitlab-ee/issues/5196){:target="_blank"}
        - [アップロードファイルのオブジェクトストレージへの移行時にファイルが漏れるバグを修正](https://gitlab.com/gitlab-org/gitlab-ee/issues/7108){:target="_blank"}
        - [プライマリノードリポジトリの検証で必ず正しいチェックサムを取得できるように修正](https://gitlab.com/gitlab-org/gitlab-ee/issues/7213){:target="_blank"}
        - [Sidekiqキューの安定化でデータの完全性を保証](https://gitlab.com/gitlab-org/gitlab-ee/issues/7279){:target="_blank"}

        [how we built GitLab Geo](https://about.gitlab.com/2018/09/14/how-we-built-gitlab-geo/){:target="_blank"}のブログ記事も参照ください。

    - name: "GeoでSSHのGitコマンドをプライマリノードにプロキシ"
      available_in: [premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/administration/geo/replication/configuration.html'
      image_url: '/images/11_4/geo_ssh_proxy_improvements.png'
      reporter: akaemmerle
      team: geo
      issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/7541'
      description: |
        地理的に分散したチームをサポートするために、可能な限りGeoの利用を簡単することを目指して開発が行われています。
        [GitLab 11.3](https://about.gitlab.com/2018/09/22/gitlab-11-3-released/#geo-improvements){:target="_blank"}では、
        はじめてセカンダリノードからSSHの`git push`をプライマリノードにプロキシすることをサポートしました。

        このリリースからは、この機能のユーザビリティとパフォーマンスを更に改善し、cloneとpullもできるようになりました。
        これにより、Geoを利用する際に、複数のリモートURLを手作業で設定する必要がなくなります。

    - name: "GitLab Runner 11.4"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/runner'
      documentation_text: "Read through the documentation of GitLab Runner"
      description: |
        GitLab Runner 11.4も同時にリリースされました。
        GitLab RunnerはCI/CDのジョブを実行し、その結果をGitLabに返すために使用される、オープンソースプロジェクトです。

        #### 注目すべき変更点:

        - [JSON形式のログ出力をサポート](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1020){:target="_blank"}
        - [対話的なウェブターミナルのサポートにdockerを追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1008){:target="_blank"}
        - [対話的なウェブターミナルのサポートにdocker machineを追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1046){:target="_blank"}
        - [docker entrypointを無効化の上書きを許可](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/965){:target="_blank"}
        - [Runnerのconcurrentとlimitの設定値をメトリクスに追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1019){:target="_blank"}
        - [gitlab_runner_jobs_totalをメトリクスに追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1018){:target="_blank"}
        - [gitlab_runner_jobs_totalをメトリクスに追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1018){:target="_blank"}
        - [job duration histogramをメトリクスに追加](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1025){:target="_blank"}
        - [Fix command and args assignment when creating containers with K8S executor](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1010){:target="_blank"}
        - [K8S executorでコンテナを作成する時のcommandとargsの割当の不具合を修正](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1010){:target="_blank"}

        GitLab Runnerのすべての変更は[CHANGELOG](https://gitlab.com/gitlab-org/gitlab-runner/blob/v11.4.0/CHANGELOG.md){:target="_blank"}を参照ください。

# Omnibus and performance

    - name: "Omnibusの改善"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/omnibus/'
      reporter: joshlambert
      team: distribution
      description: |
        - `redis`が3.2.12へアップデートされ、深刻なセキュリティの問題といくつかの脆弱性が修正されました。11.4にアップグレードした後に、`gitlab-ctl restart redis`を実行し、確実に新しいバージョンを利用してください。
        - GitLab 11.4は[Mattermost 5.3](https://mattermost.com/blog/mattermost-5-3-enhanced-search-on-desktop-and-mobile-plugin-hackathon-highlights-and-more/){:target="_blank"}を同梱します。
          [オープンソースのSlack代替品](https://mattermost.com/){:target="_blank"}で、最新リリースではデスクトップ、およびモバイルアプリでの検索機能の強化などの様々な改善がされています。
          また、[セキュリティアップデート](https://about.mattermost.com/security-updates/){:target="_blank"}も含まれているので、アップグレードすることを推奨します。
        - `git`を2.18.1へ、`libpng`を1.6.35へアップデートしました。
        - `gnupg`を2.2.10へ、`gpgme`を1.10.0へ、`libgcrypt`を1.8.3へ、`npth`を1.6へ、`libgpg-error`を1.32へ、`libassuan`を2.5.1へアップデートしました。
        - `trusted_certs`ディレクトリ内の証明書のパーミッションを`0755`から`0644`に変更しました。

    - name: "パフォーマンスの改善"
      available_in: [core, starter, premium, ultimate]
      performance_url: 'https://gitlab.com/groups/gitlab-org/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name%5B%5D=performance&milestone_title=11.4' # Link to the merged MRs in the corresponding milestone
      reporter: multiple
      team: multiple
      description: |
        GitLab 11.4にはいくつかの注目すべきパフォーマンスの改善が含まれています:

        - [多くのラベルの参照を含むマークダウンの描画を高速化](https://gitlab.com/gitlab-org/gitlab-ce/issues/48221){:target="_blank"}
        - [多くのプロジェクト間コミットの参照を含む課題のディスカッションを高速化](https://gitlab.com/gitlab-org/gitlab-ce/issues/43094){:target="_blank"}
        - [より少ないクエリ実行で課題の関連するブランチを取得](https://gitlab.com/gitlab-org/gitlab-ce/issues/43097){:target="_blank"}
        - [より少ないクエリ実行でマージリクエストの環境を取得](https://gitlab.com/gitlab-org/gitlab-ce/issues/43109){:target="_blank"}
        - [より少ないクエリ実行で通知メールの宛先を計算](https://gitlab.com/gitlab-org/gitlab-ce/issues/47496){:target="_blank"}
        - [マージリクエストで差分ディスカッションの新規作成を高速化](https://gitlab.com/gitlab-org/gitlab-ce/issues/49002){:target="_blank"}
        - [より少ないGitaly呼び出しでツリー表示の「最終コミット」情報を取得](https://gitlab.com/gitlab-org/gitlab-ce/issues/37433){:target="_blank"}
        - [デッドコードを削除後にマージリクエストの読み込みを高速化](https://gitlab.com/gitlab-org/gitlab-ce/issues/51172){:target="_blank"}

# MVP
mvp:
  fullname: "Luke Picciau"
  gitlab: Qwertie
  description: |
    Lukeさんは2段階認証のリカバリーコードのバックアップを簡単にできるように、リカバリーコードをファイルとしてダウンロードできるようにしました。
    2段階認証機能のリカバリーコードは、スマートフォンを紛失した場合などに、GitLabのアカウントにアクセスするのに必要となります。

# COVER IMAGE LICENCE
cover_img:
  image_url: 'https://unsplash.com/photos/vn58Tq0L-bc'
  licence: Unsplash
  licence_url: 'https://unsplash.com/license'

# CTA BUTTONS
# cta:
#   - title: "Join us for an upcoming event"
#     link: '/events/'
#   - title: "Sign up for the 11.4 Release Radar webcast"
#     link: '/webcast/monthly-release/gitlab-11.4---collaboration/'

# UPGRADE BAROMETER
barometer:
  reporter: release-man
  description: |
    GitLab 11.3の最新バージョンからGitLab 11.4へアップグレードする際に、ダウンタイムは発生しません。
    アップグレードでのダウンタイムを発生させたくない場合は、[ダウンタイムレスアップグレードのドキュメント](https://docs.gitlab.com/ee/update/README.html#upgrading-without-downtime){:target="_blank"}を確認してください。

    このリリースではマイグレーション、デプロイ後マイグレーションが必要です。
    大規模なマイグレーションが必要な場合は、バックグラウンドジョブとしてマイグレーションが実行されます。

    GitLab.comのマイグレーションには約34分、デプロイ後マイグレーションには合計で約2分かかりました。

    GitLab Geoのユーザーは、[Geoのアップグレードのドキュメント](https://docs.gitlab.com/ee/administration/geo/replication/updating_the_geo_nodes.html){:target="_blank"}を確認してください。

    セットアップの簡易化のため、[Omniauthがデフォルトで有効化されます](https://docs.gitlab.com/ee/integration/omniauth.html){:target="_blank"}。
    外部プロバイダは自動的に設定されないので、ほとんどの環境に影響はありません。
    ただし、以前に外部プロバイダを設定し、かつ現在はOmniauthを無効化している環境では、11.4にアップグレードするとOmniauthが有効化されるので注意が必要です。
    これを避けるには、プロバイダの設定を削除するか、[明示的にOmniauthを無効化](https://docs.gitlab.com/ee/integration/omniauth.html#disabling-omniauth){:target="_blank"}する必要があります。

# DEPRECATIONS
deprecations:
  - feature_name: "GitLab RunnerがサポートするDockerのバージョン"
    due: 2018年10月22日
    reporter: jlenny
    description: |
      GitLab 11.4(2018年10月22日)で、Dockerの最新の[推奨バージョンガイドライン](https://docs.docker.com/develop/sdk/#api-version-matrix)に従い、
      Docker 1.12(API version 1.24)以前のバージョンは非推奨となります。
      11.4のリリース後は、これら古いバージョンのDockerの公式サポートは打ち切りとなり、動作しなくなる可能性があります。
  - feature_name: "Omnibus GitLabでのPrometheus 1.xのサポート"
    due: GitLab 12.0
    reporter: joshlambert
    description: |
      GitLab 11.4(2018年10月22日)からOmniauth GitLabに同梱されるPrometheus 1.0は非推奨となります。今後はPrometheus 2.0が同梱されますが、メトリクスのフォーマットは1.0と互換性がありません。
      既存のインスタンスは[同梱されるツール](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#11-4)を使用して2.0へアップグレード、およびオプションでデータの移行ができます。

      GitLab 12.0では、Prometheus 2.0が使用されていない場合は自動的にアップグレードされます。その時に、Prometheus 1.0のデータ移行は行なわれないため、データは消去されます。
